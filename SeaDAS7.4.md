# SeaDAS 7.4 installation


This installation procedure was tested on Mac OS X Lion (10.7), Mac OS X
Mountain Lion (10.8), Mac OS X El Capitain (10.11), MacOS Sierra (10.12)  and Ubuntu 14.04LTS. This installation procedure
was tested with SeaDAS 7.4

<br/>This procedure installs SeaDAS for only one user


### <br/>Reference

* <https://seadas.gsfc.nasa.gov/downloads/>

### <br/> Notes

* Replace etienneouellet with your username.

### <br/>Prerequistes
<font color="red"> IMPORTANT </font>
<br/>

Anaconda,Python 2.7, git 2.11.1 

* https://gitlab.com/Takuvik/resources_public/blob/master/Python2Anaconda.md



### <br/>Procedure

1.  Remove old SeaDAS version.

    a.  Remove seadas6.

    b.  Remove `~/.seadas`.

    c.  Remove lines making reference to SeaDAS in the system startup
        files : .bash_profile, .bashrc and .profile.

2.  Go to <https://seadas.gsfc.nasa.gov/downloads/>

3.  Download `seadas_7.4_macos_installer.dmg`

4.  Double-click `seadas_7.4_macos_installer.dmg`

5.  Double-click SeaDAS Installer icon in the window seadas.

6.  Choose Open from the pop-up menu.

7.  Select Open.

    a.  If the window "_Install can not be opened because it is from an
        unidentified developer_" appears

    *You have to unblock it in System Preferences --) Security & Privacy --) General --) open Anyway

8.  Follow the instructions in the SeaDAS Setup Wizard.

    a.  Select Next.

    b.  Select I accept the agreement.

    c.  Select Next.

    d.  Enter in Destination directory: `/Users/etienneouellet/Applications/seadas-7.4`

    *Note: **Don’t** accept the default directory if it’s a different path

    e.  Select Next.

    f.  In the window Select File Associations

    *Make sure the box SeaDAS session file is checked. It is used to restore a SeaDAS session (*.seadas).

    g. Select Next.

    h. In the window Completing the SeaDAS Setup Wizard : Select Finish.

9.  Put the seadas icon in the dock.

    a.  In the Finder, go to `~/Applications`.

    b.  Open `seadas-7.4/bin` folder.

    c.  Click and drag the seadas icon to the dock.

10. Install Processing programs and source code from manual installation
    as explained in <https://seadas.gsfc.nasa.gov/downloads/> -&gt;
    SeaDAS Processing Programs and Source Code -&gt; Manual Installation.

    a.  `cd ~/seadas-7.4`

    b.  `rm -fr ocssw`

    c.  Download the installer script to
        `/Users/etienneouellet/Applications/seadas-7.4`

    d.  `chmod +x install_ocssw.py`

    e.  Enter the following command on only one line
    
        
    ```
    ./install_ocssw.py --install-dir=/Applications/seadas-7.4/ocssw --git-branch=v7.4 --clean --aquarius --aqua --avhrr --czcs --goci --hico --meris --terra --mos --ocm1 --ocm2 --octs --oli --osmi --seawifs --viirsn --direct-broadcast --eval

    ```        
   

11. Make sure your .bash_profile contains

	
    ```
    source ~/.profile

    ```	

    Add the following two lines to your `.profile`:
	
    
    ```
        
    export OCSSWROOT=/Users/etienneouellet/Applications/seadas-7.4/ocssw
    source $OCSSWROOT/OCSSW_bash.env

    ```
    
    
12. Restart the terminal.

<br/> **Troubleshooting**   

<br/>**Problem 1**: Error at OCSSW &gt; Install OC Processors using the GUI.

For example,

```
Error - Executing command 
"cd /Users/maximebenoit-gagne/Applications/seadas-7.4/ocssw; curl -O --retry
5 --retry-delay 5 https://oceandata.sci.gsfc.nasa.gov/ocssw/bundles.sha256sum"

```
Bundle checksum file (bundles.sha256sum) not downloaded

<br/>Solution: Do the manual installation instead.

1.  Restart procedures from step 10.

<br/>**Problem 2:** Error at the manual installation of OC processing programs
and source code.

For example:

```
Error - Could not run "cd /Applications/seadas-7.4/ocssw/run/data/common; git fetch -q &gt; /dev/null"

```
Solution: Use the git from Anaconda Python.



1.  `cd ~/seadas-7.4`

2.  `rm -fr ocssw`

3.  See
    svn/Takuvik/Teledetection/SOP/Python/Python2/Python2Anaconda.docx

    a.  `conda install -c anaconda git=2.11.1`

    b.  At the line Proceed `([y]/n)`?, Enter `y`.
	        

4.  Restart procedures from step 10.

<br/><br/><br/>

_The Takuvik Remote Sensing Team_  :)