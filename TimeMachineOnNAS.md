# Time Machine backup on NAS


<br>**Procedure**

*How to configure backup on a iomega NAS*



1.  Format the drive to NTFS format

2.  Enable AFP service

    1.  Log on the NAS

    2.  Select backup

    3.  Select Time Machine

        1.  Enable Time Machine Share

3.  Go to Lenovo web site and download LenovoEMC Storage Manager for Mac

    1.  Open LenovoEMC Storage Manager

    2.  Choose a password

4.  Connect to the NAS

    1.  Log as Admin

    2.  Select Common

    3.  Select the Users Tab

    4.  Select the new account ---) Selects the box Administrator ---)
        Apply

    5.  On the Access Permisions tab ---) add access permissions you
        want

5.  Log on NAS as admin

    1.  Select backup

    2.  Select Time Machine

        1.  In Settings chose Destination Share

    3.  Go back to Time MachinTab

        1.  Selects Add a Time Machine backup folder

        2. Apple Network Hostname = Computer Name

            1.  /System Preferences/Sharing

        2. Apple Ethernet ID = Mac address

            1.  To find your MAC address enter ifconfig in teterminal

6.  On your Mac, select Finder and choose Go &gt; Connect to Serverfrom
    the Menu Bar

    1.  Put the IP address

        1.  Connect as Registred User

            1.  Check the Remembre this password in my keychain box

                1.  Select the volume you want to mount on

7.  Open System Preferences

    1.  Go to Users & Groups

        1.  Selects the account

            1.  In Loging Items

                1.  Add the volume you want and check Hide box


