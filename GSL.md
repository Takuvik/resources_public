This installation procedure was tested on Mac OS X Lion (10.7) and Mac
OS X Yosemite (10.10).

Prerequisite

1.  MacPorts. See https://gitlab.com/Takuvik/resources_public/blob/master/MacPorts.md



Install GSL.

  `$ sudo port install gsl`


