# Create a branch with SmartSVN



*This procedure creates the branch
<https://svn.fsg.ulaval.ca/svn/Takuvik/Teledetection/Couleur/Quotidien/branches/mybranch>
from the local
/Users/maximebenoit-gagne/svn/Takuvik/Teledetection/Couleur/Quotidien/trunk.*

*Replace the absolute name of the directories and the files in function
of what you want to do.*

*This procedure was tested with SmartSVN 7.6 on Mac OS X 10.7 (Lion).*

<be>**Prerequisite**

1.  The directory
    <https://svn.fsg.ulaval.ca/svn/Takuvik/Teledetection/Couleur/Quotidien/branches>
    already exists both locally and on the SVN repository.

<br>**Procedure**

1.  Copy.

    1.  Open SmartSVN.

    2.  In the window “What do you want to do?”

        1.  Make sure both “Open existing project” and the project
            containing the trunk directory are selected.

        2. Click OK.

    3.  In the SmartSVN window

        1.  Select Teledetection/Couleur/Quotidien/trunk.

    4.  Modify &gt; Copy…

    5.  In the window “Copy directory (keeping history)”

        1.  In the “Target Directroy” field, make sure
            Couleur/Quotidien/ branches is entered.

        2. In the “New Name” field, enter mybranch.

        3. Click Copy.

2.  Update.

    1.  In the SmartSVN window

        1.  Select Teledetection/Couleur/Quotidien/branches/mybranch.

        2. Click Update.

    2.  In the window “Update working copy”

        1.  Make sure both “Update To:” HEAD and “Depth:” Fully
            recursive are selected.

        2. Click Update.

3.  Commit.

    1.  In the SmartSVN window,

        1.  Select Teledetection/Couleur/Quotidien/branches/mybranch.

        2. Click Commit.

    2.  In the “Configuration” panel,

        1.  Make sure “Depth: Fully recursive” is selected.

        2. Click Continue.

    3.  In the “Files” panel

        1.  In the field “Commit Message:”, enter a message.

        2. Click Commit.


