# Configure Tivoli


**This installation procedure was tested on Mac OS X Yosemite (10.10).**

<br>**Reference**

[*https://www.computecanada.ca/page-daccueil-du-portail-de-recherche/portail-globus-de-calcul-canada/globus-user-documentation/?lang=fr*](https://www.computecanada.ca/page-daccueil-du-portail-de-recherche/portail-globus-de-calcul-canada/globus-user-documentation/?lang=fr)

[*https://wiki.calculquebec.ca/w/Globus\#tab=tab2*](https://wiki.calculquebec.ca/w/Globus#tab=tab2)

<br>**Prerequisites**

1.  An account on Briarée.
    See https://gitlab.com/Takuvik/resources_public/blob/master/Briaree.md


<br>**Procedure**

1.  Download Tivoli. Go to
    [*http://www-01.ibm.com/support/docview.wss?&uid=swg24036670*](http://www-01.ibm.com/support/docview.wss?&uid=swg24036670).

2.  Ask Maxime the private document *Configuration des clients TSM –
    Google Docs.pdf*.

    1.  Notes

        1.  Enter sudo dsmj instead of only dsmj


