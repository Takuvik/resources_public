
# Transfer Files with Globus

*This procedure allows you to transfer files across the differen Compute Canada resources*

<br>Prerequistes

1. Compute Canada account

<br>Procedure

1. Follow this link : <https://globus.computecanada.ca>
2. Select "Compute Canada" in the drop-down
    1. Your "existing organizational login" is your CCDB account
3. Click on either one of the two "Endpoint" fields, and start typing the site name in the top box.
    1. All Compute Canada resources have names prefixed with computecanada#
    2. For example, computecanada#cedar-dtn or computecanada#graham-dtn.

4. Now select a second endpoint and authenticate it the same way.
5. Highlight a file or directory that you want to transfer by single-clicking on it.
6. Globus provides several other options in the "Transfer Settings" area at the bottom of the Transfer Files page. Choose the ones that fit your needs

Source: Compute Canada Wiki



