# Some solutions to possible Mail Postfix problems



*Tested on Yosemite (10.10).*

1.  Set Mail Postfix to run permanently

    a.  <http://apple.stackexchange.com/questions/32228/whats-the-correctly-way-to-make-postfix-run-permanently-on-lion-not-server>

2.  Start Mail Postfix (if it is not running yet)

    a.  Type command: sudo postfix start in a terminal


