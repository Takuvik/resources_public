# Install SWIG for Python


*This installation procedure was tested on Mac OS X High sierra (10.13).*

<br>**Reference**

<http://www.swig.org/>

<https://support.leapmotion.com/entries/39433657-Generating-a-Python-3-3-0-Wrapper-with-SWIG-2-0-9>

<br>**Prerequisite**

* Homebrew. See [https://gitlab.com/Takuvik/resources_public/blob/master/Homebrew.md]().

<br>**Procedure**

1.  Install SWIG.
    1.  `brew install swig`
    


