# Install Gfortran

This installation procedure was tested on Mac OS X Lion (10.7), Mac OS X Yosemite (10.10) and Mac OS X Sierra (10.12).

<br>**Prerequisite**

1. Xcode. See [https://gitlab.com/Takuvik/resources_public/blob/master/Xcode.md]().


<br>

**Install gfortran**

    
This installation procedure was tested on Mac OS X Sierra (10.12).
 
1. Dowload and install gfortran 6.3 from 	[link](http://coudert.name/software/gfortran-6.3-Sierra.dmg)  

2. Right click on the file gfortran-6.3-Sierra.dmg and then click open. If instead you double click it, you will most likely get a message telling “… can’t be opened because it is from an unidentified developer” and you will be stuck.

3.  Make this gfortran the default gfortran on your system.

    1. `sudo mkdir /gfortran_dir`
    2. `sudo ln -s /usr/local/gfortran/bin/gfortran /gfortran_dir/gfortran`
    3. Add the following line to your .bash_profile:
    
        `export PATH="/gfortran_dir:$PATH"`

    Please note that the corresponding library is:
`/usr/local/gfortran/lib/libgfortran.a`
    
This installation procedure was tested on Mac OS X Lion (10.7) and Mac OS X Yosemite (10.10).  

1.  Dowload gcc-42-5666.3-darwin11.pkg from [link](http://r.research.att.com/tools/). Please note that at the moment of writing these lines (June 4^th^ 2015), the latest version available is for Mac OS X Lion 10.7. It is still going to work on Yosemite 10.10.

2. Right click on the file gcc-42-5666.3-darwin11.pkg and then click open. If instead you double click it, you will most likely get a message telling “… can’t be opened because it is from an unidentified developer” and you will be stuck.

3.  Make this gfortran the default gfortran on your system.

    1. `sudo mkdir /gfortran_dir`
    2. `sudo ln -s /usr/bin/gfortran-4.2 /gfortran_dir/gfortran`
    3. Add the following line to your .bash_profile:
    
        `export PATH="/gfortran_dir:$PATH"`

    Please note that the corresponding library is:
`/usr/lib/gcc/i686-apple-darwin11/4.2.1/x86_64/libgfortran.a`

