# HDFview installation


*This installation procedure was tested on Mac OS X Mountain Lion (10.8)
and Mac OS X Yosemite (10.10).*

<br>**Prerequisite**

1.  Java. <See https://gitlab.com/Takuvik/resources_public/blob/master/Java8.md>



    a.  Download HDF-Java 2.11 Pre-Built Binary Distributions for Mac OS X
    on the following website:
    <https://www.hdfgroup.org/products/java/release/download.html>

    b.  Follow the typical Mac OS application installation and launch the
    application from your application folder once it is installed.


