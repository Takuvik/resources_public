# Install Google Earth Pro



*This installation procedure was tested on Mac OS X Moutain (10.8).*

<br>**Reference**

https://www.google.com/earth/download/gep/agree.html

<br>**Procedure**

1.  Download.

    a.  Go to https://www.google.com/earth/.

    b.  Download Google Earth Pro

2.  Install.

3.  License key.

    1. Start Google Earth Pro.

    2. In the window “Welcome to Google Earth”

    3. Enter in the Username field: &lt;your\_email&gt;.

    4. Replace &lt;your\_email&gt; by your email.

    5. Enter in the License Key field: GEPFREE

    6. Tick “Enable automatic login.”.


