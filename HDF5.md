# HSF5 Installation

*This installation procedure was tested on Mac OS X Lion (10.7) and Mac
OS X Yosemite (10.10).*

<br>**Prerequisite**

1.  MacPorts. See <https://gitlab.com/Takuvik/resources_public/blob/master/MacPorts.md>



<br>**Install HDF5.**

`$ sudo port install hdf5`


