# Install MacPorts

*This installation procedure was tested on Mac OS X Lion (10.7), Mac OS X
Mountain Lion (10.8) and Mac OS X Yosemite (10.10).*

<br>**Prerequistes**

1.  Xcode. See https://gitlab.com/Takuvik/resources_public/blob/master/Xcode4.md with Mac OS X Lion (10.7) or
   https://gitlab.com/Takuvik/resources_public/blob/master/Xcode5for10_8.md with Mac OS X Mountain Lion (10.8) or
   https://gitlab.com/Takuvik/resources_public/blob/master/Xcode5for10_10.md with Mac OS X Yosemite (10.10).



1.  Install MacPorts.
   

    a.  Go to <http://www.macports.org/install.php>.

    b.  Install MacPorts.


