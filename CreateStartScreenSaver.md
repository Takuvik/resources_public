*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

<https://discussions.apple.com/thread/2188359?start=0&tstart=0> See the
answer of BobHarris on Oct 8, 2009 12:30 PM.

<br>**Procedure**

1.  Start Automator.

    a.  In the Finder, Go &gt; Applications &gt; Automator.

2.  Select Application.

3.  Drag Start Screen Saver to the grey area.

4.  File &gt; Save to Desktop with the name startscreensaver.


