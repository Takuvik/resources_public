# Install and configure Rstudio in a Docker on mac OS

Pull the image from Docker Hub

    docker pull dceoy/rstudio-server
    
Default username and password:

username: `rstudio`

password: `rstudio`

Run a server 

    docker container run --rm -p 8787:8787 -v ${PWD}:/home/rstudio -w /home/rstudio dceoy/rstudio-server

### Add Persisting User Data

    
Make a shell connection to the container.

    docker ps
    
Find the good ID adn shell connect

    docker exec -t -i <ID> /bin/bash
    
Use mkpasswd command tu encrypt password

    apt-get update && apt-get install -y whois
    
    
Create users

    useradd <user> -m -p `mkpasswd <password>` -s /bin/bash