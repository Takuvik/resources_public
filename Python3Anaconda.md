# Install Python 3


*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*


<br>**Procedure**

1.  Install Anaconda.

    1.  Go to <https://www.continuum.io/downloads\#\_macosx>.

    2.  Download Anaconda for OS X Python 3.4 Mac OS X 64-bit
        Graphical Installer.

    3.  Install.


