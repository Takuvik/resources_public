# Upgrade to Mac OS X Yosemite (10.10)



*This installation procedure was not tested yet.*



<br>**Reference**

1.  <https://www.apple.com/ca/osx/how-to-upgrade/>

2.  <http://trac.macports.org/wiki/Migration>

<br>**Prerequistes**

1.  Apple ID.

2.  Internet connection.

    
<br>**Procedure**



1.  Backup.

    1.  Do a Time Machine of the system on an external hard drive.

    2.  Disconnect the external hard drive and keep its content until
        you are sure you will not want to downgrade back to Mac OS X
        Lion (10.7).

2.  Upgrade.

    1.  Follow the instructions on
        <https://www.apple.com/ca/osx/how-to-upgrade>.

3.  Migrate a MacPorts installation. Enter each command on only
    one line. Don’t install other software (like R, RStudio, …) at the
    same time. Restart from step b if you have to interrupt
    the installation.

    1.  `$ port -qv installed > myports.txt`

    2.  `$ sudo port -f uninstall installed`

    3.  `$ sudo rm -rf /opt/local/var/macports/build/*`

    4.  `$ curl -O <https://svn.macports.org/repository/macports/contrib/restore_ports/restore_ports.tcl>`

    5.  `$ chmod +x restore_ports.tcl`

    6.  `$ sudo ./restore_ports.tcl myports.txt`

    7.  `$ sudo port select --set python python27`

    8.  `$ sudo port select --set python2 python27`

    9.  `$ sudo port select --set cython cython27`

4.  Install Java for OS X 2015-001. (For SmartSVN 7.6).

    1.  See https://support.apple.com/kb/DL1572?locale=en\_US.

5.  If you have SeaDAS 6,

    1.  Follow the instructions
        on svn/Takuvik/Teledetection/SOP/SeaDAS/SeaDAS6.4\_on\_Yosemite.docx.

6.  Some users report having needed to reinstall Dropbox.

7.  Turn on mails (mostly for those who use terminal to
    send/receive mails)

    1.  Type the following command in the terminal: sudo
        /usr/sbin/postfix start

8.  Mount the volumes to mount with automount.
    See svn/Takuvik/Teledetection/SOP/Administration/MountDiskStationUserAutomount.docx.

<br>**Troubleshooting**

1.  You get the message

    Waiting for lock on /opt/local/var/macports/registry/.registry.lock

    during the step Migrate a MacPorts installation, enter

   ` $ sudo rm /opt/local/var/macports/registry/.registry.lock`


