# Configure SwitchResX

*This installation procedure was tested on Mac OS X Mountain El Capitan (10.11).*



## Procedure

1. Configure SwitchResX

    1. System Preferences --) SwitchResX
    
    2. A popup will appear "SwitchResX helper modules"
        * Check the 2 boxes --) ok

    3. On the tab *About SwitchResX*, uncheck the box "Check online for update"

    4. On the tab *General Settings* check the box "Launch SwitchResX Deamon automatically after login"
        * In the scroll-down menu select "last used resolutions"

    5. On the tab *Menus* check the box "is active and contains" in the column "Menu Extra"




 
   
