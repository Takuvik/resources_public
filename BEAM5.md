
# Installation of BEAM 5.0

This installation procedure was tested on Mac OS X Lion (10.7).

<br>Prerequistes

1.  None.

<br>Procedure

1.  Download.

    1.  Go to <http://www.brockmann-consult.de/cms/web/beam/>.

    2.  Select Downloads.

    3.  Download the Installer for Mac OS X.

2.  Install.

    1.  Double-click beam\_5.0\_mac\_installer.dmg.

    2.  Double-Click the BEAM Installer.

    3.  Click Open.

    4.  Accept the agreement.

    5.  Accept the default options in the installation wizard.

3.  Put the BEAM icon in the dock.

    1.  In the Finder, go to Applications.

    2.  Open the beam-5.0/bin folder.

    3.  Drag and drop the visat icon to the dock.


