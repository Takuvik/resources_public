# Install Python 2 of Canopy



*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

svn/Takuvik/Teledetection/brucemonger/2015/Setting\\ Up\\ SeaDAS\\ and\\
Python\\ A2015.docx

<br>**Note**

Canopy is a product of Enthought.

<br>**Procedure**

1.  Download Canopy.

    1.  Go to <https://store.enthought.com\#canopy-academic>.

    2.  Click **Request your license** button.

    3.  Register with your ulaval.ca email.

    4.  You will receive an email. Click on the link in this email to
        activate your Enthought account.

    5.  You will be brought to the web page
        <https://store.enthought.com/licenses/academic/request/>, click
        on the **here** link.

    6.  You will be brought to the web page
        <https://store.enthought.com/downloads/#default>, click the
        **DOWNLOAD Canopy** button.

2.  Install Canopy.

    1.  Double-click canopy-1.5.5-osx-64.dmg from your
        Downloads directory.

    2.  Follow the instructions.

3.  Configure Canopy.

    1.  Start Canopy.

        1.  Applications &gt; Canopy.

    2.  In the Canopy Environment Setup,

        1.  Accept the defaults and click Continue. This will install
            Canopy only for your user.

    3.  In the Make Canopy your default Python environment?,

        1.  Accept the defaults and click Start using Canopy.

4.  Install packages.

    1.  Start Canopy.

    2.  Click Package Manager icon.

    3.  Install the following packages:

        1.  HERE. See
            svn/Takuvik/Teledetection/brucemonger/2015/Setting\ Up\ SeaDAS\ and\Python\A2015.docx, p. 2.



