# Install Python 2 of MacPorts


*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

<https://astrofrog.github.io/macports-python/>

<https://trac.macports.org/wiki/Python>

<br>**Prerequisite**

MacPorts: See svn/Takuvik/Teledetection/SOP/MacPorts/MacPorts.docx.

<br<**Procedure**

1.  Installing packages.

    1.  `sudo port selfupdate`

    2.  `sudo port install python27`

    3.  `sudo port install py27-matplotlib py27-numpy py27-scipy py27-ipython`

2.  Configuration.

    1.  Create a folder called .matplotlib in your home directory and
        copy the default matplotlibrc file to it. (Write on one line.)

        ```
        cp /opt/local/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/matplotlib/mpl-data/matplotlibrc ~/.matplotlib/

        ```
    2.  Edit `.matplotlib/matplotlibrc` and change the following line

        ```
        backend : Agg

        ```

        to
        
        ```
        backend : MacOSX

        ```

    3. This ensures that when you use Matplotlib interactively, a
        window will pop up.

    4.  Optionally, you can uncomment and set the image.origin and
        image.interpolation lines to

        ```
        image.origin : lower

        image.interpolation : nearest
        ```

    5.  Make this Python installation the default:

        1. `sudo port select --set python python27`

        2. `sudo port select --set ipython ipython27`


