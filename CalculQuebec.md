*This installation procedure was tested on Mac OS X Yosemite (10.10).*

<br>**Reference**

<http://www.calculquebec.ca/en/access-to-resources>

<https://wiki.calculquebec.ca/w/Connecting_and_transferring_files>

<br>**Prerequisites**

1.  Account on Calcul Canada.
    See https://gitlab.com/Takuvik/resources/blob/master/CalculCanada.md

Notes

<br>**Procedure**

1.  Register to Calcul Québec.

    1.  Go to <https://ccdb.computecanada.ca/security/login>.

    2.  `Login with your username and password of Compute Canada.`

    3.  Request an account on Calcul Québec.

    4.  Wait for a confirmation.

    5.  Note your username and password of Calcul Québec.


