# l3bindump2 installation


*This installation procedure was tested on Mac OS X Lion (10.7) and Mac
OS X Moutain Lion (10.8).*

<br>**Prerequisite**

*HDF4. <https://gitlab.com/Takuvik/resources_public/blob/master/HDF4.md> *

<br>**Procedure**

1.  Build l3bindump2.

`$ cd ~/svn/Takuvik/Teledetection/Util/l3bindump2`

`$ make`
