# Installation of takuvik.R



This installation procedure was tested on Mac OS X Lion (10.7) and Mac
OS X Moutain Lion (10.8).

<br>**Prerequisite**

* RStudio.

<br>**Procedure**

1.  Get code.

    1.  Install the R package ncdf with RStudio.

2.  To use a function in takuvik.R.

    1.  Source svn/Takuvik/Teledetection/Util/takuvik.R in your
        R script.


