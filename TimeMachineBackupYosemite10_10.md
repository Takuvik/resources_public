# Time Machine backup Yosemite 10.10



*This installation procedure was tested on Mac OS X Yosemite (10.10).*



<br>**Reference**

 <https://support.apple.com/en-ca/HT201250>

<br>**Prerequistes**

1.  External hard drive

<br>**Procedure**


1.  Connect the external hard drive to your MacBook.

2.  In System Preferences, choose Time Machine.

3.  Click Choose disk.

4.  Choose disk of your choice where you want to backup your files.

5.  Click Use disk.

6.  Wait until files have been copied.


