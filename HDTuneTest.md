# HD Tune to test hard drive disks


1.  Plug the HDD to the computer.

2.  Open HD Tune software.

3.  Start an « Error Scan »

    a.  In the dropdown menu on the top left, select the disk to test

    b.  Among the tabs, click the « Error Scan » tab.

    c.  Click Start on the right.


