# Install Mou

*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>

1.  Go to http://25.io/mou.

    a.  Click Download.

    b.  Double-click the .zip.

    c.  In the Finder, drag Downloads/Mou to Applications/Mou.


