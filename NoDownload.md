# NoDownload


*This installation procedure was tested on Mac OS X Mountain Lion (10.8)*

*with Microsoft Outlook for Mac 2011. It is used to not automatically
download attached files in Microsoft Outlook. Note that when a message
is selected, the attached files will be downloaded.*

<br>**Reference**

<https://support.office.com/en-us/article/Exchange-account-server-settings-5025680d-5b3e-441e-93ed-c073a82cc364>

<br>**Prerequisites**

1.  Microsoft Outlook for Mac.

<br>**Procedure**


1.  Start Microsoft Outlook.

2.  Outlook &gt; Preferences &gt; Accounts &gt; Advanced.

3.  Tick **Download headers only**.


