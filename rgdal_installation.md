# Install rgdal package


*This procedure explains how to install the rgdal package.*

*This installation procedure was tested on Mac OS X Yosemite (10.10) and
R 3.2.2.*

1.  Download and install GDAL Complete frameworks for MacOS X

    1.  Go to <http://www.kyngchaos.com/software:frameworks> and
        download GDAL Complete frameworks.

    2.  Install GDAL Complete frameworks.

        i.  Go to Downloads folder

        ii. Click GDAL_Complete-X.XX.dmg

        iii. Double click GDAL Complete.pkg

        iv. Follow the instructions

2.  Download rgdal

    a.  Go to <https://cran.r-project.org/src/contrib/Archive/rgdal/>

    b.  Download the latest version available (name of the file to
        download should resemble to rgdal_1.0-5.tar.gz)

3.  Install the package

    a.  Use the command below (in only one whole line) where you change
        « path-to-the-file-downloaded-at-step-2-b » in yellow. In my
        case it was changed to «
        /Users/alexandrepetit/Downloads/rgdal_1.0-5.tar.gz ».

```
R CMD INSTALL /*path-to-the-file-download-at-step-2-b/*
--configure-args='--with-gdal-config=/Library/Frameworks/GDAL.framework/Programs/gdal-config
--with-proj-include=/Library/Frameworks/PROJ.framework/Headers
--with-proj-lib=/Library/Frameworks/PROJ.framework/unix/lib'

```