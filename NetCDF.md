# Installation NetCDF

*This installation procedure was tested on Mac
OS X Yosemite (10.10).*

<br>**Prerequisite**

* Homebrew. [https://gitlab.com/Takuvik/resources_public/blob/master/Homebrew.md]().

<br>**Install NetCDF.**
    
1. `brew tap Homebrew/core`
2. `brew install netcdf` It took me one hour and fifteen minutes. It installs hdf5 automatically also.
3. install cdo following the instruction at this address: https://code.mpimet.mpg.de/projects/cdo/embedded/index.html#x1-30001.1 
4. `brew cask install java`
5. `brew install nco` It installs gsl automatically also.
6. `brew cask install xquartz`
7. `brew install ncview`
8. Install HDFView (which works with NetCDF too).
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/HDFview.md]().


