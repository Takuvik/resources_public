# Configure Globus


*This installation procedure was tested on Mac OS X Yosemite (10.10).*


<br>**Goal**

Create a connection between a MacBook Pro and a Compute Canada server to
transfer files.

<br>**Reference**

<https://docs.google.com/presentation/d/1KywJHs6g5AfWs-PF0WcFMlcUqTbDyMHNCnJp82ldyUo/edit?pref=2&pli=1#slide=id.ga56b31db6_1_0>
Slides 5 to 22 (in French)

<br>**Prerequisites**

Account on Compute Canada server. See <https://gitlab.com/Takuvik/resources/blob/master/Colosse.md`>

Notes

1.  These procedures use Colosse as a Compute Canada server but can be
    applied to any Compute Canada server by using the appropriate
    Compute Canada server name.

    Procedure



2.  Register to Globus.

    a.  Go to <https://www.globus.org/SignUp#>.

    b.  Fill the form.

    c.  Note your Globus username and password.

3.  Configure Globus to create a connection between your MacBook Pro
    and Colosse. Follow the procedures in the slides 5 to 22 (in French)
    at
    <https://docs.google.com/presentation/d/1KywJHs6g5AfWs-PF0WcFMlcUqTbDyMHNCnJp82ldyUo/edit?pref=2&pli=1#slide=id.ga56b31db6_5_8>.

    a.  At slide 11, The link *Get Globus Connect Personal* has moved.

    1. Select Manage Data &gt; Console.

    2. Select Dashboard.

    3. Select Globus Connect Personnal.

    b.  At slide 12, note the endpoint (point de chute) name you enter
        for your MacBook Pro. It will be referred below as your MacBook
        Pro’s endpoint name.

    c.  At slide 18,

    1. Manage Data &gt; Transfer files.

    2. The *start transfer* link is not there. Instead click in the left Endpoint text box.

    3. Enter your MacBook Pro’s endpoint name.

    4. Select files.

    5. Click in the right Endpoint text box.

    6. Enter computecanada #colosse-dtn2

    7. Click the right arrow.


