# Install smos-ee-to-netcdf

*This installation procedure was tested on Mac OS X Lion (10.7).*

<br>**Reference** 

https://earth.esa.int/web/guest/-/data-reader-software-7633

<br>**Prerequistes**

1.  Java 8. See https://gitlab.com/Takuvik/resources_public/blob/master/Java8.md

<br>Procedure



1.  Download.

    1.  Go to
        <https://earth.esa.int/web/guest/-/data-reader-software-7633>.

    2.  Click on “To download the SMOS NetCDF Conversion Tool click
        [here](http://org.esa.s3tbx.s3.amazonaws.com/software/installers/v1.0.1/smos-ee-to-netcdf-standalone.zip)”.

    3.  `$ cd <current_folder>`


        &lt;current_folder&gt; is the folder from which you want to
        call smos-ee-to-nc.sh.

    4.  `$ mv ~/Downloads/smos-ee-to-netcdf-standalone.zip `

    5.  `$ unzip smos-ee-to-netcdf-standalone.zip`

    6.  `$ cd smos-ee-to-netcdf`

2.  Test.

    1.  `$ ./smos-ee-to-nc.sh`

        The beginning of the output on the Terminal should be:

        usage: smos-ee-to-nc [options] file ...

        Options:

        --compression-level &lt;int&gt; Target file compression level. 0
        = no …


