# Get start with Gitlab

 <br>**Prerequisites**

 1. You will need a gitlab acount. Here <https://gitlab.com/users/sign_in>
 
 **References**

* [https://help.github.com/articles/checking-for-existing-ssh-keys/]().
* [https://help.github.com/articles/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent/]().
* [https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/]().

<br>**Procedure**

1. You have to ask a administrator to grant you access rights to the Takuvik Group

2. Install git on your computer

    3. Download and install git from this source <https://sourceforge.net/projects/git-osx-installer/>

3. Set up user name and user email

    1. `git config --global user.name <your user name>`

    2. `git config --global user.email <your email address>`
    
4. Generate ssh key and add it to the ssh-agent.
    1. [Check for existing ssh keys](https://help.github.com/articles/checking-for-existing-ssh-keys/).
    
    1. `ssh-keygen -t rsa -b 4096 -C “your_gitlab@email.com”`
    
    2. `eval "$(ssh-agent -s)”`
    
    3. `ssh-add -K ~/.ssh/id_rsa`
    
5. Add the ssh key to your GitLab account.

    1. `pbcopy < ~/.ssh/id_rsa.pub`
    
    2. Connect to your GitLab account > Settings > SSH Keys.
    
    3. Paste the clipboard content to the Key field.
    
    4. Write a title for example the name of your computer in the Title field.
    
    5. Click Add Key.

6. Creat a working directory for git on your computer

    1. `cd` Just to be sure you are in your home directory

    2. `mkdir gitlab`

7. Clone the gitlab projects

    1. Go to your gitlab directory

        1. `cd gitlab`

    2. Clone the gitlab project    

        1. for "ressources_public" : `git clone https://gitlab.com/Takuvik/resources_public.git`

        2. for "ressources" : `git clone https://gitlab.com/Takuvik/resources`

        3. for "examples" : `git clone https://gitlab.com/Takuvik/examples.git`

8. Basic command that covers 80% of your needs

    1. Example : I want to add a README.md file to a project.

        1. Go to the project folder in the terminal

        2. Creat the file 

            1. `nano README.md`

        3. Update your local directory

            1. `git pull origin master`

        4. Commit the change

            1. `git commit -m "add README.md"`

        5. Push the change to the server

            1. `git push origin master`

        6. At anytime you can check your git status. Usefull if you get error message.

            1. `git status`