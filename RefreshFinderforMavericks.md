# Install Refresh Finder on Mac OS X Mavericks (10.9)


*This installation procedure was not yet tested on Mac OS X Mavericks
(10.9).*

<br>**Reference**

<http://apple.stackexchange.com/questions/49543/is-there-a-way-to-refresh-a-finder-file-listing>

See the comment of Barranka.

<br>**Procedure**

1.  Install Refresh Finder.

    2.  Go to http://www.soderhavet.com/refresh/.

    3.  Download Refresh Finder
        1.4.0.dmg]<http://www.soderhavet.com/refresh/Refresh_Finder_1.4.0.dmg.zip>

    4.  Double click the .dmg file.

2.  Move the “Refresh Finder” icon to the Applications directory.

3.  In the Finder,

    1.  Click on the Applications directory.

4.  Press [cmd]+[alt] as you drag the “Refresh Finder” icon from the
    Applications directory to the Finder toolbar.

    Maybe [cmd] is sufficient instead of [cmd]+[alt].


