# TimeMachine from MacBook Pro to NAS recover



*This procedure explains how to make a system recovery from a TimeMachine
that is on a NAS.*

*This installation procedure was tested on Mac OS X Yosemite (10.10).*

1.  Connect the NAS to your computer using an Ethernet cable.

    1.  Turn off the NAS (using the GUI)

    2.  Turn off the MacBook Pro

    3.  Unplug the Ethernet cable from the NAS’ socket 1

    4.  Plug an Ethernet cable from the NAS’ socket 1 to the MacBook Pro

    5.  Turn on the Macbook Pro

    6.  Turn on the NAS

    7.  Note the IP address showing on the NAS. Show be 169.254.\*.\*

    8.  Mount the NAS onto the MacBook Pro. See
       https://gitlab.com/Takuvik/resources/blob/master/MountDiskStationUserAutomount.md

        1.  The NAS should now appear in /Volumes of the MacBook Pro

2.  Copy the TimeMachine files from the NAS to an external hard drive

    1.  Locate the NAS in /Volumes/«NAS name»

    2.  Locate the TimeMachine files on the NAS

    3.  Open a terminal

        1.  Using the « cp » command, copy the TimeMachine to the
            connected external hard drive. For instance, if the files
            are in the path “/Volumes/TimeMachineFiles/” and the
            external hard drive is located at “/Volumes/externalHDD/”,
            enter the following command in the terminal to copy the
            TimeMachine files from the NAS to the external hard drive:
            “cp –R /Volumes/TimeMachineFiles/ /Volumes/externalHDD/”

3.  Use the backup files to backup the system

    1.  Connect the external hard drive to the MacBook Pro

    2.  Restart the MacBook Pro holding down Command and R keys (Cmd +
        R) simultaneously

    3.  In the Recovery window, select the option to Restore from Time
        Machine Backup

    4.  Select the Time Machine backup disk (the external hard drive)

    5.  Select the Time Machine backup you want to restore (the files
        copied in step 2)

    6.  Click Continue and follow the onscreen instructions to reinstall
        OS X using the backup files


