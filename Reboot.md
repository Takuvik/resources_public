# Reboot taku-leifr and taku-eirikr

<br>**Prerequisite**

https://gitlab.com/Takuvik/resources_public/blob/master/CreateStartScreenSaver.md

<br>**Procedure**

1. Reboot the Mac Pro


    1.  `sudo shutdown -r now`

2.  Lock screen on the physical monitor to hide the actions of a user of the Mac Pro from people in Vachon-2076
   

    1.  Share screen.

    2.  Log as takuvikulaval. See Maxime for the password.

    3.  Double-click startscreensaver.app on the Desktop.

    4.  Quit share screen.


