
# Upgrade R packages


*This procedure explains how to upgrade R via RStudio.*

*This installation procedure was tested on Mac OS X Yosemite (10.10).*

<br>**Procedure**

1.  Install XQuartz

    1.  Go to the following link: <http://www.xquartz.org/>

    2.  Download XQuartz by clicking the .dmg file. The file name should
        resemble to “XQuartz-2.7.8.dmg”

    3.  Once downloaded, double click on the file and follow the
        instructions to install it. It is pretty straightforward,
        nothing special to mention about the installation.

2.  Install R

    1.  Go to the following link:
        <https://cran.r-project.org/bin/macosx/>

    2.  Download the binary package (.pkg file) for Mac OS X. The file
        name should resemble to “R-3.2.2.pkg”.

    3.  Once downloaded, double click on the file and follow the
        instructions to install it. It is also a pretty straightforward,
        nothing special to mention about the installation.

3.  Upgrade your packages

    1.  Open RStudio

    2.  Copy the packages from the old version library to the new
        version library. An example of the command to use is
        shown belown. In this example, R was upgraded from version 3.0
        to version 3.2.

    ```
    cp –R */Library/Frameworks/R.framework/Versions/3.0/Resources/library /Library/Frameworks/R.framework/Versions/3.2/Resources/library

    ```
    3.  Type `update.packages()`

    4.  The packages from the old version should now be installed and R
        should be fully upgraded


