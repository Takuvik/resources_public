# Install FileZilla client

This installation procedure was tested on Mac OS X Moutain (10.8) and on Mac OS X Yosemite (10.10).

**Note**
Don’t download FileZilla_3.dmg. It contains malware even if it is the first installer you will see on the official site of FileZilla. See [https://en.wikipedia.org/wiki/FileZilla#Bundled_adware_issues]().

**Reference**

* [http://academe.co.uk/2013/12/filezilla-without-the-spyware/]().

**Procedure**

1. Download.
    1. Go to [https://filezilla-project.org/download.php?show_all=1]().
    2. Download FileZilla_3.27.1_macosx-x86.app.tar.bz2.
2. Install.
