# Install Ubuntu in VirtualBox



*This installation procedure was tested on Mac OS X Mountain Lion (10.8)
with Ubuntu 12.04.*

*Reference can be found at
<http://osxdaily.com/2012/03/27/install-run-ubuntu-linux-virtualbox/>.
There is one little but important modification to do on this page.
Select version Ubuntu (64-bit) instead of version Ubuntu.*

1.  Download and install VirtualBox.
 
    1.  Uninstall any previous installation of VirtualBox.

        2.  In the Terminal,

            1.  `$ cd`

            2.  `$ rm -r VirtualBox\\ VMs/`

            3.  `$ cd`

            4.  `$ cd Library`

            5.  `$ rm -r VirtualBox/`

        3. Start Applications&gt;VirtualBox\_Uninstall.tool.

    2.  Go to
        [www.virtualbox.org/wiki/Downloads](http://www.virtualbox.org/wiki/Downloads).

    3.  Download VirtualBox 4.3.10 for OS X hosts.

    4.  Double-click on VirtualBox-4.3.6-93012-OSX.dmg.

    5.  Follow the instructions of the installation wizard. Accept
        the defaults.



2.  Download and install a 64-bit Ubuntu Linux ISO.
   

    1.  Go to <http://www.ubuntu.com/download/desktop>.

    2.  Select Ubuntu 12.04 LTS 64-bit.

    3.  Select “Not now, take me to the download”.

    4.  In the window “Opening Ubuntu-12.04.4-desktop-amd64.iso”

        1.  Select Save File.

        2. Wait until the download completes. The progression can be
            monitored in the Firefox progress bar in the dock.

2.  Create a new virtual machine.
   

    1.  Start Applications&gt;VirtualBox.

    2.  Click “New”.

    3.  In the window “Name and operating system”

        1.  In the field “Name”, enter Ubuntu.

        2. In the drop-down menu “Type”, select Linux.

        3. In the drop-down menu “Version”, select Ubuntu (64-bit).

    4.  In the window “Memory size”

        1.  Accept the defaults and click “Continue”.

    5.  In the window “Hard drive”

        1.  Accept the defaults (“Create a virtual hard drive now”) and
            click “Create”.

    6.  In the window “Hard drive file type”

        1.  Accept the defaults (“VDI (VirtualBox Disk Image)”) and
            click “Continue”.

    7.  In the window “Storage on physical hard drive”

        1.  Accept the defaults (“Dynamically allocated”) and
            click “Continue”.

    8.  In the window “File location and size”

        1.  Accept the defaults and click “Create”.

3.  Install Ubuntu in the virtual machine.
    
    1.  In the VirtualBox Manager screen

        1.  Click “Settings”.

    2.  In the “General” tab,

        1.  Click on the “Storage” tab.

    3.  In the “Storage” tab

        1.  Next to Controller: IDE click the + icon that looks like a
            CD to add a new IDE Controller.

        2. Click “Choose Disk”.

        3. Select the Ubuntu
            ISO (Downloads/Ubuntu-12.04.4-desktop-amd64.iso).

        4. Select “Open”.

        5.  Select “OK”.

    4.  In the VirtualBox Manage screen

        1.  Select “Start”.

    5.  In Ubuntu

        1.  Select “Install Ubuntu”.

    6.  In “Preparing to install Ubuntu” window

        1.  Select “Download updates while installing” and “Install this
            third-party software” and select “Continue”.

    7.  In the “Installation type” window

        1.  Accept the defaults (“Erase disk and install Ubuntu”) and
            select “Continue”.

    8.  In the “Erase disk and install Ubuntu” window

        1.  Accept the defaults and select “Install Now”.

    9.  In the “Where are you?” window

        1.  Accept the defaults (“Toronto”) and select “Continue”.

    10.  In the “Keyboard layout” window

        1.  Accept the defaults (“English (US) – Englis (US)”) and
            select “Continue”.

    11.  In the “Who are you?” window, modify the following accordingly
        to who you are

        1.  Your name: Maxime Benoit-Gagne.

        2. Your computer’s name: maxime-VirtualBox.

        3. Pick a username: maximebenoit-gagne.

        4. Choose a password: \*\*\*\*\*\*\*\*.

        5. Confirm your password: \*\*\*\*\*\*\*\*.

        6. Select Log in automatically.

    12.  Wait for the installation to complete. It takes somewhere
        between 20 and 45 minutes.

    13.  When the window “Installation Complete” with a “Restart Now”
        button appears, close Ubuntu by clicking the red circle at the
        upper left of the “Ubuntu \[Running\]” window.

    14.  In the “You want to:” window

        1.  Select “Power off the machine”.

    15.  In the VirtualBox Manager screen

        1.  Select “Start”.

            Note: The preceding steps m, n and o will be called “Reboot
            Ubuntu” from now.

    16.  In Ubuntu

        1.  Close the window that prompts you to install Ubuntu. Ubuntu
            is already installed.

4.  Install the Guest Additions.

    See [www.virtualbox.org/manual](http://www.virtualbox.org/manual)
    Section 4.2.2 Guest Additions for Linux.

    In the Ubuntu Terminal,

    1. `$ sudo apt-get update`

    2.  `$ sudo apt-get upgrade`

    3.  `$ sudo apt-get install dkms`

    4.  Reboot Ubuntu.

    5.  Select Devices&gt;Insert Guest Additions CD image…

    6.  `$ cd /media/VBOXADDITIONS_4.3.10_93012`

    7.  `$ sh ./VBoxLinuxAdditions.run`

5.  Create a shared folder.


    See [www.virtualbox.org/manual](http://www.virtualbox.org/manual)
    Section 4.3 Shared folders.

    In the Mac Terminal

    1.  `$ cd`

    2.  `$ mkdir shared`

    3.  Reboot Ubuntu.

        * In Ubuntu

    4.  Select Devices &gt; Shared Folders Settings

    5.  Select the + icon that looks like a folder.

    6.  In the “Add Share” window, in the “Folder Path” drop-down menu,
        select “Other”.

    7.  Select /Users/maximebenoit-gagne/shared and click “Choose”.

    8.  Back to the “Add Share” window, select “Make Permanent” and
        click “OK”.

    8.  Back to the “Shared Folders” tab, click “OK”.

        In the Ubuntu Terminal

    9.  `$ cd`

    10. `$ mkdir shared`

    11. `$ sudo nano /etc/rc.local`

    14.  Add the following entry to the /etc/rc.local file **on one
        line** just before exit 0:

       1.  `sudo`

       2. `/opt/VBoxGuestAdditions-4.3.10/lib/VBoxGuestAdditions/mount.vboxsf`

       3. `shared /home/maximebenoit-gagne/shared`

       4.  WriteOut by pressing Control-O.

       5. Exit by pressing Control-X.

       6. Reboot Ubuntu.


