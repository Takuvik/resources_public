# Ncks installation

*This installation procedure was tested on Mac OS X Yosemite (10.10).*

<br>**Procedure**

1.  Install ncks.

 
<br>**Troubleshooting**

1.  For the error message:

    ```
    dyld: Library not loaded: /opt/local/lib/libgsl.0.dylib

    Referenced from: /opt/local/bin/ncks

    Reason: image not found
    ```
    
<br>**Solution**

`$ sudo ln -s /opt/local/lib/libgsl.19.dylib /opt/local/lib/libgsl.0.dylib`
