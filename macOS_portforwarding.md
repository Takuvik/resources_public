# MacOS  port forwarding

### For testing 


    echo "
    rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080
    " | sudo pfctl -ef -



### Persistent

    sudo touch /private/etc/pf.anchors/org.user.forwarding

    rdr pass inet proto tcp from any to any port 80 -> 127.0.0.1 port 8080
    
    sudo nano /private/etc/pf.conf 

Original file:


    scrub-anchor "com.apple/*"
    nat-anchor "com.apple/*"
    rdr-anchor "com.apple/*"
    dummynet-anchor "com.apple/*"
    anchor "com.apple/*"
    load anchor "com.apple" from "/etc/pf.anchors/com.apple"
    
to


    scrub-anchor "com.apple/*"
    nat-anchor "com.apple/*"
    rdr-anchor "com.apple/*"
    rdr-anchor "org.user.forwarding"
    dummynet-anchor "com.apple/*"
    anchor "com.apple/*"
    load anchor "com.apple" from "/etc/pf.anchors/com.apple"
    load anchor "org.user.forwarding" from "/etc/pf.anchors/org.user.forwarding"
    
Test yout file 

    sudo pfctl -vnf /etc/pf.anchors/org.user.forwarding