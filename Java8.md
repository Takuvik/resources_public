# Java 8 instalation

*This installation procedure was tested on Mac OS X Lion (10.7).*

<br>**Procedure**
1.	Download.
    1.	Go to java.com/en/download.
    2.	Click “Free Java Download”.
    3.	Click “Agree and Start Free Download”.
    4.	Download jre-8u45-macosx-x64.dmg.
2.	Install.
    1.	See <https://java.com/en/download/help/mac_install.xml.>
3.	` $ sudo ln -s /Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java /opt/local/bin/java`
4.	Test.
    1.	Restart the browser.
    2.	Go to <https://java.com/en/download/installed.jsp>
    3.	Click « Verify Java version ».
