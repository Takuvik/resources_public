# Quotas

<br>**Good usage**



The quota of each user on the shared Mac Pro taku-leifr is:

20 GB on /Users/\<username\> (backed up)

100 GB on /Volumes/rap/\<username\> (backed up)

?? GB on /Volumes/scratch (not backed up)

<br>*Replace \<username\> with your username on taku-leifr.*
