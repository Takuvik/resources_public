# How to kill a process when you can not access the terminal

*This installation procedure was tested on Mac OS X Yosemite (10.10.5).*

<br>**Procedure**

1. Find your process PID

    1. Open Activity Monitor 
    
    2. Look on the list and remember the PID of the process you want to kill

2. Kill the process with Automator 

    1. Go to Launchpad --) Other --) Automator

    2. A window will pop "Choose a type for your document"

        1. Select "Application"
    
    3. Scroll down in the list

        1. Select "Run Shell Script"

    4. In the apropriate box put : `$echo <password> | sudo -S <command>`

        1. Click "Run" in the top right






