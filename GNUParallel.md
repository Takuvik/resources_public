# Install GNU Parallel


*This installation procedure was tested on Mac OS X Lion (10.7).*

<br>**Prerequisite**

1.  MacPorts. See <https://gitlab.com/Takuvik/resources_public/blob/master/MacPorts.md>.


<br>**Install GNU Parallel.**
    

`$ sudo port install parallel`


