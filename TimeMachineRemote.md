# Access the Time Machine backups on a remote Mac Pro




*This procedure explains how to access the Time Machine backups on a
remote Mac Pro.*

*This installation procedure was tested on Mac OS X Lion (10.7).*

*Replace *maximebenoit-gagne* by your username.*

<br>**Procedure**

1.  Enter Time Machine.

    1.  Select the Time Machine icon on the menu bar.

    2.  Select “Enter Time Machine.” You will **not** see the Time
        Machine Graphical User Interface. Don’t be worried. This step
        will make the directory “Time Machine Backups” appear in
        the Terminal.

2.  Search in Time Machine Backups.

    1.  Open a Terminal.

    2.  \$ cd /Volumes/Time Machine Backups/Backups.backupdb/taku-eirikr

    3.  Search the file you want.

    4.  Copy this file to your home directory
        (/Users/*maximebenoit-gagne*) with cd.


