# Gpredict satellite tracking software installation, V1.1





Mac OS X Yosemite + MacPorts + Iridium setup

Eric Rehm

28 October 2015

Gpredict is a real-time satellite tracking and orbit prediction
application. It can track an unlimited number of satellites and display
their positions. Gpredict can also predict the time of future passes for
a satellite, and provide you with detailed information about each pass.
[*http://gpredict.oz9aec.net/index.php*](http://gpredict.oz9aec.net/index.php)

<br>**Preparation (See <https://guide.macports.org/>)**

1.  Install Xcode (Mac App Store)

2.  Command line tools OS X for Xcode
    (https://developer.apple.com/downloads/, requires login via an
    Apple ID)

3.  Install Macports (https://guide.macports.org/)

4.  sudo port selfupdate

<br>**Gpredict installation sur Mac OS X Yosemite (10.10.5) avec MacPorts**
*(méthode essaie-erreur):*

1. Install XQuartz (X11 on OS X)

2. Install gpredict (ouf !!!):

    `sudo port install glib2`

    `sudo port install gdk-pixbuf2`

    `sudo su`

    `sh-3.2# gdk-pixbuf-query-loaders > /opt/local/lib/gdk-pixbuf-2.0/2.10.0/loaders.cache`

    `sh-3.2# exit`

    `sudo port install gpredict` # This may take some time and You may need to do this several times

3. Run gpredict    

4. ericrehm% **gpredict** &

5. Edit &gt; Update TLE &gt; From Network

6. File &gt; New Module

7. Module Name: **Iridium**

8. Group &gt; **Iridium**

9. // Double click on each Iridium Satellite to add to “Selected Satellites”

10. OK

11. Edit &gt; Preferences &gt; Modules

12. Select layout: “All views (wide)” or “Polar and single sat"

13. 10. OK

14. Edit &gt; Preferences &gt; General &gt; Ground Stations Tab  // Add a ground station corresponding to glider position, e.g., 

15. Add New

16. Name: **BaffinBay**

17. Description: **Central Baffin Bay**

18. Location: **Central Baffin Bay**

19. Latitude: **70.78 N**

20. Longitude: **64.14 W**

21. Altitude: **0**

22. (Dialog will update Locator automatically)

23. Select BaffinBay as Ground Station, click on Default box.

24. OK

25. File &gt; Exit  // Exit gpredict and restart gpredict to see new layout and ground station

26. Polar plot is centered on the ground station. Azimuth rings at increments of 30°. Satellites closer to polar origin (higher elevation) are better. Click on any satellite in polar view to determine time to LOS (loss of signal, i.e., satellite below horizon).


-   ***TBD:  How to zoom map to Baffin Bay***

See also:

Manual: [*http://sourceforge.net/projects/gpredict/files/Gpredict/1.3/gpredict-user-manual-1.3.pdf/download*](http://sourceforge.net/projects/gpredict/files/Gpredict/1.3/gpredict-user-manual-1.3.pdf/download)

[*https://docs.fedoraproject.org/en-US/Fedora/19/html/Amateur_Radio_Guide/others-gpredict.html*](https://docs.fedoraproject.org/en-US/Fedora/19/html/Amateur_Radio_Guide/others-gpredict.html)

[*https://www.youtube.com/watch?v=UUCatIOERu8*](https://www.youtube.com/watch?v=UUCatIOERu8)

