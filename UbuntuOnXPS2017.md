**Dual boot install ubuntu on Dell XPS 2017**

*Be sure to have  kernel > 4.4*


1. Update Bios Dell XPS 15 9560 System BIOS to Version 1.5.0 and Graphics card: nVidia n17P-G0
    1.http://www.dell.com/support/home/ca/en/cabsdt1/product-support/product/xps-15-9560-laptop/drivers


2. Follow the steps explained in the link below

    https://github.com/rcasero/doc/wiki/Ubuntu-linux-on-Dell-XPS-15-(9560)

3. Fix palm rejection : https://stevenkohlmeyer.com/fixing-palm-detect-ubuntu-14-04/
4. Install gnome-shell

    ```
    sudo apt-get install ubuntu-gnome-desktop
    sudo apt-get remove unity
    sudo apt-get remove ubuntu-desktop
    ```

5. Repaire sources.list
    1. https://askubuntu.com/questions/863933/ubuntu-16-04-messed-sources-list
    2. sudo apt-get update
6. Removing Hot Corner on gnome-shell
    1. https://extensions.gnome.org/extension/118/no-topleft-hot-corner/