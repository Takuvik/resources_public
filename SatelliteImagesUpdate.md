1.  MODIS-Aqua

    a.  Modify and
        run svn/Takuvik/Teledetection/Get/MODISA/get\_modis\_L3BIN.sh.

2.  MODIS-Atmosphere

    a.  Use FileZilla.

        (ftp://ladsweb.nascom.nasa.gov/allData/51/MYD08_D3).

3.  NSIDC

    a.  Use FileZilla.
        (<ftp://sidads.colorado.edu/pub/DATASETS/nsidc0051_gsfc_nasateam_seaice/>
        or ftp://sidads.colorado.edu/pub/DATASETS/nsidc0081\_nrt\_nasateam\_seaice/).

    b.  If a new year of data from the final data set [*Sea Ice
        Concentrations from Nimbus-7 SMMR and DMSP SSM/I-SSMIS Passive
        Microwave Data*](http://nsidc.org/data/nsidc-0051.html) (0051)
        becomes available, modify
        svn/Takuvik/Teledetection/Couleur/Quotidien/trunk/lance_prodprim.sh
        to use this final data set instead of the temporary data set
        [Near-Real-Time DMSP SSM/I-SSMIS Daily Polar Gridded Sea Ice
        Concentrations](http://nsidc.org/data/nsidc-0081.html) (0081).

4.  Update documentation.

    a.  Update /Volumes/taku-njall/README.txt.


