# General Installation

This installation procedure was tested on Mac OS X Yosemite (10.10).

## Procedure

1. Configure a backup strategy and backup. I use CrashPlan. Note that it is a paid solution. You can use TimeMachine on an external hard drive if you have one instead.
2. Update the operating system.
    1. Apple Menu > Software Update > Continue.
3. Install Xcode. I use Xcode 7.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/Xcode.md]().
4. Install Homebrew.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/Homebrew.md]().
5. Install HDF4. Don't install HDF5. It will be installed automatically with NetCDF. I had error messages with my programs when I installed HDF5 separately from NetCDF.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/HDF4.md]().
6. Install NetCDF.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/NetCDF.md]().
7. Install Gfortran.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/Gfortran.md]().
8. Install GMT4.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/GMT4.md]().
9. Install Python 2.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/Python2Anaconda.md]().
10. Install SWIG.
    1. See [https://gitlab.com/Takuvik/resources_public/blob/master/swig.md]().
 
   