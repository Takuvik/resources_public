# Change User ID


*Tested on Mac OS X Yosemite (10.10)*

**Ideally you should backup your data first.**


<br>**Procedure**


<br> **1. Make a random user and give this new random user administration
rights.**

1.  System preferences

2.  Users & Groups

3.  Click the lock to makes changes and enter password

4.  Click the « + » button.

5.  In the window:

    a.  New Account: select « Administrator »

    b.  Full Name: Bidon

    c.  Account name: Bidon

    d.  Click « Create User »

6.  Log onto the new user « Bidon » you just created.

<br>**2. Change UID**

Read the uid (given Alice as the user's name, 501 as old and 1234 as the
new uid):

Root permissions followed by password:

`$ sudo -s`

Verify current uid:

```
# dscl . -read /Users/Alice UniqueID\
UniqueID: 501 

```
Change uid:


`# dscl . -change /Users/Alice UniqueID 501 1234`


Verify that the uid has changed:

` # dscl . -read /Users/Alice UniqueID\`

`UniqueID: 1234`

**3. Change ownership of the user's files**

```
# find /Users/Alice -user 501 -print0 | xargs -0 chown -h 1234\
# find /Library -user 501 -print0 | xargs -0 chown -h 1234\
# find /Applications -user 501 -print0 | xargs -0 chown -h 1234\
# find /usr -user 501 -print0 | xargs -0 chown -h 1234\
# find /private/var/ -user 501 -print0 | xargs -0 chown -h 1234

```
If you want to be sure that you changed the ownership of all files of
the root partition ("Macintosh HD" or whatever you named it), you could
do the following (but be prepared that this takes considerably longer,
especially if you have much data in /Users):

`# find -xP / -user 501 -print0 | xargs -0 chown -h 1234`

A simple test if there are files left that are owned by the old uid:

`# find -xP / -user 501 -ls`

**4. Rename special files and folders**

But that was not all. Mac OS X has some special files and folders that
have the (old) uid as part of their names. These include (on my Mac,
ymmv):

`/.Trashes/501`

`/Library/Caches/com.apple.ImageCaptureExtension2.ICADeviceDatabase.501`

`/Library/Caches/com.apple.ImageCaptureNotifications.DeviceDiscoveryDatabase.501`

`/private/var/db/launchd.db/com.apple.launchd.peruser.501`

and possibly some files in `/private/var/folders/ud/(some ugly dir name)/-Caches-/`

For every of the above you have to use the « mv » command and change the
number (501 in this example) to the new uid (1234 in this example).

`# mv /.Trashes/501 /.Trashes/1234`

If you want to check if there are remaining files or directories that
have the old uid in their name, you can, again, use « find » to list the
files (example below) and then rename them using « mv » (just like
above):

`# find -xL / -name "\*501"`

**5. Finalize: reboot**

Reboot your computer.
