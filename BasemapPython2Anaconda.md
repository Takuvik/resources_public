# BasemapPython2Anaconda


This installation procedure was tested on Mac OS X Mountain Lion (10.8)
with Python 2.7.11, Anaconda 2.4.0, GCC 4.2.1.

<br>**Reference**

<https://anaconda.org/mutirri/pyhdf>

<br>**Prerequisites**

* Python 2 from Anaconda : https://gitlab.com/Takuvik/resources_public/blob/master/MacPorts.md


**Procedure**



1. `conda install basemap`


