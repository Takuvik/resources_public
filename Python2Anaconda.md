# Python2Anaconda installation


*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

<https://github.com/spencerahill/aospy/issues/58>

<br>**Procedure**

1.  Install Anaconda.

    1.  Go to <https://www.continuum.io/downloads>.

    2.  Download Anaconda for OS X Python 2.7, Mac OS X 64-bit
        Graphical Installer.

    3.  Double click the .pkg file and follow the instructions on
        the screen.

2.  Quit and start the Terminal.

3.  Install some packages.

    1.  netcdf4

        1.  conda install -c conda-forge netcdf4

    2.  git

        1.  `conda install -c anaconda git=2.11.1`

        2. At the line Proceed (\[y\]/n)?,

            1.  Enter y.


