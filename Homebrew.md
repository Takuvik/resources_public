# Install Homebrew

This installation procedure was tested on Mac OS X Yosemite (10.10).

## Reference
* [https://brew.sh/](https://brew.sh/)


## Prerequisites
1. Xcode. See [https://gitlab.com/Takuvik/resources_public/blob/master/Xcode.md](https://gitlab.com/Takuvik/resources_public/blob/master/Xcode.md)

## Procedure
1. `/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`
2. `brew install caskroom/cask/brew-cask`
3. `brew cask install xquartz`

