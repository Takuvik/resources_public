# Install Texmaker


*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

http://www.howtotex.com/howto/installing-latex-on-mac-os-x/

<br>**Procedure**

1.  Install MacTeX.

    1.  Go to <http://tug.org/mactex/>.

    2.  Download MacTeX.

    3.  Install.

2.  Install Texmaker.

    1.  Go to <http://www.xm1math.net/texmaker/>.

    2.  Go to the Download section in the menu on the right.

    3.  Download Texmaker 4.4.1 for MacOSX.

    4.  Install.


