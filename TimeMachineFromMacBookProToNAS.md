# TimeMachine from MacBook Pro to NAS recover



*This procedure explains how to move a TimeMachine from a MacBook Pro to
a NAS.*

*This installation procedure was tested on Mac OS X Yosemite (10.10).*

*Please note that you can automate this process. See file
\~/svn/Takuvik/Teledetection/Teledetection/SOP/TimeMachine/TimeMachineOnNAS.docx*

1.  Create a share on the NAS (requires admin rights). This is where the
    TimeMachine files will be copied.

2.  Connect the external hard drive containing the TimeMachine files to
    taku-eirikr

3.  Log onto taku-eirikr from your MacBook Pro. Once logged onto
    taku-eirikr, you will have access to both the external hard drive
    containing the TimeMachine files and the NAS to which you want to
    copy the TimeMachine files.

4.  Copy the TimeMachine files to the NAS

    1.  Open a terminal

    2.  Locate the TimeMachine files. Should be somewhere in
        /Volumes/«Your external hard drive name»

    3.  Locate the NAS. Should be somewhere in /Volumes/«NAS name»

    4.  Use the « cp » command to copy your TimeMachine files to the NAS
        share you created in step 1. This step might take several hours
        to complete.


