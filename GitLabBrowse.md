# Browse GitLab Takuvik/Procedures repository
<br>

This installation procedure was tested on Mac OS X Mountain Lion (10.8).

<br>**Reference**

[link](https://gitlab.com/>)


<br>**Procedure**

1.  Create a GitLab account.

    a.  Go to [link](https://gitlab.com)

    b.  Click Sign in.

    c.  Create an account.

    d.  Note your GitLab username, your GitLab email and your
        GitLab password.

2.  Communicate your GitLab username to the owner of the project
    Takuvik/ resources (Maxime).

3.  Wait an email from Maxime confirming that an owner of the group
    Takuvik added you to the group Takuvik with a status of Reporter
    or more.

4.  You can now browse the project

    a.  Go to at [link](https://gitlab.com/Takuvik/resources/tree/master)

    b.  Enter your GitLab email.

    c.  Enter your GitLab password.


