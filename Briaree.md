This installation procedure was tested on Mac OS X Yosemite (10.10).

Reference

<http://www.calculquebec.ca/en/access-to-resources>

<https://wiki.calculquebec.ca/w/Connecting_and_transferring_files>

<br>**Prerequisites**

1.  Registration to Calcul Québec.
    https://gitlab.com/Takuvik/resources_public/blob/master/CalculQuebec.md


<br>**Procedure**

1.  Register to Briarée.

    a.  Go
        <https://portail.calculquebec.ca/accounts/login/?next=/common/dashboard/>.

    b.  Login with your username and password of Calcul Québec.

    c.  Select My Profile &gt; Servers account.

    d.  Check Available for access Briarée Add resource’s access.

    e.  Leave the defaults for other options.

    f.  Click Submit.

    g.  Wait for a confirmation.

    h.  Note that the username and password for Briarée are those of
        Calcul Québec.


