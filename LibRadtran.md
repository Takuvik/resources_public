# HD Tune to test hard drive disks


*This installation procedure was tested on Mac OS X Lion (10.7).*

*Replace maximebenoit-gagne by your own user name on the system.* 

*$ means the terminal prompt.*

<br>**Prerequistes**

 1. Xcode https://gitlab.com/Takuvik/resources_public/blob/master/Xcode5for10_9.md

 2.  MacPorts. See https://gitlab.com/Takuvik/resources_public/blob/master/MacPorts.md

<br>**Installation**

1. Download LibRadtran.
   

    a.  Go to <http://www.libradtran.org>.

    b.  Select Download.

    c.  Select the link **most recent version 1.7**.

    d.  `$ cd`

    e.  `$ mkdir Applications`

    f.  `$ mv Downloads/libRadtran-1.7.tar.gz Applications`

2.  Unpack the distribution.

    `$ cd Applications`

    `$ gzip -d libRadtran-1.7.tar.gz`

    `$ tar –xvf libRadtran-1.7.tar`

3. Install prerequisite tools.

    `$ sudo port install gcc48`

    a.  Add the following lines to your `.bash_profile`

```
     export LDFLAGS="-L/opt/local/lib -L/usr/X11/lib"

     export CPPFLAGS="-I/opt/local/include -I/usr/X11/include"
```
a.  `$ source ~/.bash_profile`

b.  `$ sudo port install gsl`

c.  `$ sudo port install gmp`

d.  `$ sudo port install netcdf`

e. `$ sudo port install gawk`

<br>**Compile the distribution.**

a.  `$ cd libRadtran-1.7`

b.  `$ ./configure`

c.  `$ make`

<br>**Test the program.**


`$ make check`

<br>**Change your PATH.**


a.  Add the following line to your `.bash_profile`

```
    export PATH=/Users/maximebenoit-gagne/libradtran-1.7/bin:$PATH

```
b.  `$ source ~/.bash_profile`

<br>**Install the GUI.**


a.  `$ sudo port selfupdate`

b.  `$ sudo port install python27`

c.  `$ sudo port select --set python python27`

d.  `$ sudo port install py27-curl`

e.  `$ sudo port install zlib`

f.  `$ sudo port install py27-wxpython-3.0`

g.  `$ sudo port install py27-numpy`

h.  `$ sudo port install py27-matplotlib`

<br>**Test the GUI**

i.  `$ cd GUI`

j.  `$ python GUI.py`


