# Create a project built with make in Xcode

*This procedure was tested on Mac OS X Lion (10.7), Mac OS X Mountain
Lion (10.8) and Mac OS X Yosemite (10.10). This procedure was tested
with Xcode 4.6.*

<br>**Prerequistes**

1.  Xcode 4. See Xcode/Xcode4.docx.

*Replace maximebenoit-gagne by your own user name on the system. $ means the terminal prompt.*

*This procedure takes for example the following case. The primary
productivity code was already checked out from Takuvik SVN repository.
We want to create a Xcode project for that code outside the SVN tree. A
target all exists in the Makefile.*

*Acknowledgements. Eric Rehm who taught me the procedure.*

1.  Location of the Xcode project.

    a.  `$ cd`

    b. ` $ mkdir –p Xcode/svn/Takuvik/Teledetection/Couleur/Quotidien`

<!-- -->

1.  Create project.

2.  Start Xcode.

3.  In the window “Welcome”

    1.  Select “Create a new Xcode Project.”

    2. In the pane “Choose a template for your new project”

        1.  Select OS X --) Other --) External Build System.

        2.  Select Next.

4. In the pane “Choose options for your new project:”

    1.  In the “Product Name” field, enter “trunk”.

    2. In the Company Identifier field, enter “Takuvik”.

    3. Select Next.

5.  In the pane,

    1.  Select

    ```
    /Users/maximebenoit-gagne/Xcode/svn/Takuvik/Teledetection/Couleur/Quotidien

    ```

    2. Select Create.

6. Add files.

    1.  File &gt; Add Files to “trunk”…

    2.  In the pane,

    3. Make sure the 

        ```
        /Users/maximebenoit-gagne*/*svn/Takuvik/Teledetection/Couleur/Quotidien/trunk folder
        ```        
        * Is selected. Uncheck “Copy items (if needed)”.

    4. Select “Create folder references”.

    5. Make sure “trunk” is selected in “Add to targets”.

    6.  Select Add.

7.  Configure build.

    1.  Select the target trunk (at the right of the target icon).

    2.  In the Info pane,

        1.  In the Arguments field, enter $(ACTION) all

        2. In the Directory field, select '/Users/maximebenoit-gagne/svn/Takuvik/Teledetection/Couleur/Quotidien/trunk'

8.  Edit Scheme.

    1.  Product &gt; Scheme &gt; Edit Scheme.

    2.  In the Run pane,

        1.  Select the Info pane.

        2. Click the double arrows of the dropdown menu Executable.

        3. Select Other…

        4. In the window, select

        ```
        /Users/maximebenoit-gagne/svn/Takuvik/Teledetection/Couleur/Quotidien/trunk/prodprim

        ```

  * Select Choose.

  * Select OK.

9.  Test.

    1.  Product &gt; Build.

    2.  Product &gt; Run.

10.  Test with arguments. (Optional.)

    1.  Product &gt; Scheme &gt; Edit Scheme.

    2.  In the Run pane,

        1.  Select the Arguments pane.

        2.  Click the + sign under “Arguments Passed On Launch”.

        3.  Enter the first argument.

        4.  Repeat steps 7.b.i.1 and 7.b.i.3 for each argument.

        5.  Select OK.

    3.  Product &gt; Run.


