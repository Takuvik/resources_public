# HDF4 Installation


*This installation procedure was tested on Mac
OS X Yosemite (10.10).*

<br>**Prerequisite**

1. Homebrew. [https://gitlab.com/Takuvik/resources_public/blob/master/Homebrew.md]().

<br>**Install HDF4.**
    
1. `brew install homebrew/science/hdf4`


