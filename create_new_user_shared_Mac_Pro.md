# Create new user on shared Mac Pro


*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Prerequisites**
1.	the group name StudentsRemote must have been created on the computer.
    * "System preferences" -> User & Groups -> "+" (as if you were adding new account) -> Under "New account" select "Group" -> Type in group name -> "Create group"

<br>**Procedure**

1. Create new user
    * System preferences --) User & Groups --) unlock the padlock --) + --) enter required information

2. Add the new user to the group StudentsRemote
    * System preferences --) User & Groups --) StudentsRemote --) Membership --) Check the apropriate box

3. Configurer SwitchResX
    * [https://gitlab.com/Takuvik/resources_public/blob/master/SwitchResX.md](https://gitlab.com/Takuvik/resources_public/blob/master/SwitchResX.md)

4. Install SeaDAS7.4
	* <https://gitlab.com/Takuvik/resources_public/blob/master/SeaDAS7.4.md>

5. Ask Richard to fix the user IP address
	* Write it down in info/ip.xlsx

6. Mount the shared folders output-dev, output-prod, rap, scratch, taku-njall et taku-njall-L1A on the user macbook pro.
	* <https://gitlab.com/Takuvik/resources/blob/master/MountDiskStationUserAutomount.md>

7. Enter the appropriate information in Daily_root/Quotas.csv
	*	we do not want to automatically check for quota anymore. Note : but we can refer to them in the future if a concern about a lack of storage occurs.

8. Send the following mail
    * <https://gitlab.com/Takuvik/resources/blob/master/WelcomeMessage.txt>


	

   
