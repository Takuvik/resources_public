# Install GMT4

*This installation procedure was tested on Mac OS X Yosemite (10.10).*

<br>**Prerequisite**

* Homebrew. [https://gitlab.com/Takuvik/resources_public/blob/master/Homebrew.md]().

<br>**Procedure**

4. Install ghostscript.
    1. `brew install ghostscript`
1. Install gmt4.

    1.`brew install gmt4`
    2. export PATH="/usr/local/opt/gmt@4/bin:$PATH"
    3. `xyz2grd`
    
        This should print the usage of the GMT command xyz2grd on the terminal.
3. Install ImageMagick.
    1. `brew install imagemagick`

<br>*If you experience difficulties to install GMT4 or GMT5, you can try the following alternative.*

<br>

a)  Follow this link :
    <http://gmt.soest.hawaii.edu/projects/gmt/wiki/Download>

b)  Download the latest version of GMT

c)  Copy the GMT file to your applications directory

d)  Open a new terminal and edit the file .profile

e)  add the following line :

```
export PATH="/Applications/GMT5.2.1.app/Contents/Resources/bin:$PATH"

```

