# Parallels Desktop


*This installation procedure was tested on Mac OS X Mountain (10.8) with*

*Parallels Desktop 10.*

<br>**Reference**

1.  http://kb.parallels.com/4729

<br>**Procedure**

<!-- -->

1.  Key and Windows DVD.

    a.  Ask Richard for a key for Parallels. Desktop.

    b.  Ask Richard for a DVD of Windows.

2.  Get the installer.

    a.  I used ParallelsDesktop-10.2.2-29105.dmg from a
        previous download. Note that only Parallels Desktop 11 was
        available from the website at the day of writing:
        <http://www.parallels.com/ca/products/desktop/>.

3.  Install.

    a.  Double-click the \*.dmg file.

    b.  Double click Install.

    c.  Accept the license agreement.

    d.  Register.

    e.  Enter the activation key.

4.  In the window “New Virtual Machine”,

    a.  Insert the Windows DVD.

    b.  Click Continue.

    c.  Select the Windows DVD.

    d.  Click Continue.

5.  In the window “Windows Product Key”,

    a.  Leave “Express Installation” selected.

    b.  Click Continue.

6.  In the window “I will primarily use Windows for”,

    a.  Productivity.

    b.  Click Continue.

7.  In the window “Name and Location”,

    a.  Leave the defaults.

    b.  Click Continue.

8.  Drag Applications &gt; Parallels Desktop to the dock.


