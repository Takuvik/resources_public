# Refresh finder for Montain Lion

*This installation procedure was tested on Mac OS X Mountain Lion (10.8).*

<br>**Reference**

<https://astrofrog.github.io/macports-python/>

<br>**Procedure**

1.  Install Refresh Finder.

    1.  Go to <http://www.soderhavet.com/refresh/>.

    2.  Download [Refresh Finder
        1.4.0.dmg] <http://www.soderhavet.com/refresh/Refresh_Finder_1.4.0.dmg.zip>

    3.  Double click the .dmg file.

2.  Move the “Refresh Finder” icon to the Applications directory.

3.  In the Finder,

    a.  Click on the Applications directory.

4.  Drag the “Refresh Finder” icon from the Applications directory to
    the Finder toolbar.


