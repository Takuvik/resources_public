## Install Xcode

This installation procedure was tested on Mac OS X Yosemite (10.10).

<br>**Reference**

https://guide.macports.org/chunked/installing.html\#installing.xcode



<br>**Procedure**


1.  Download Xcode.

    1.  Open App Store.

        1.  Finder &gt; Applications &gt; App Store.

    2.  Write Xcode in the search bar.

    3.  Press Enter.

    4.  Select Xcode.

    5.  Select the **Install** button. If the label on the button is
        **Update** or **Installed**, Xcode is already installed.

    6.  (Facultative.) Select “Purchases” on the bar on top of the App
        Store to see the progress bar.

2.  Install Xcode.

    1.  Start Xcode.

        2.  Finder &gt; Applications &gt; Xcode.

    2.  Agree.

    3.  Enter you password.

    4.  Quit Xcode.

        1.  Xcode &gt; Quit Xcode.
        
3. Install Command Line Tools.

    1. `xcode-select --install`
    
    2. Click Install.


