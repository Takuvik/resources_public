# Takuvik data
For each set of files, a brief description and the emplacement is given. More information can be found in the README files in each emplacement.

## Atmospheric data

What: These contains "hole filled" atmosrpheric data for PPv1. Missing values (ex.: O3) have been replaced with the average calculated on the 45N+ grid.

Where: /output-prod/Takuvik/Teledetection/Products/atmosphere/

## Bathymetry and WWF province
What: Bathymetry and WWF province.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/Bathymetre/

Example: Province_Zbot_MODISA_L3binV2.nc

## Chlorophyll-a Concentration Vertical Profile
What: Chlorophyll-a concentration vertical profile.

Where: /Volumes/output-prod/Takuvik/Teledetection/All/Daily/

Examples:

* 2006/225/A2006225_chlz_00_05.nc (MODIS)
* 2006/225/G2006225_chlz_00_06.nc (GlobColour)

## Grids
What: Grids.

Where: /Volumes/output-prod/Takuvik/Teledetection/Grid/trunk/

Example:  201510151636/A45N.nc

## Ice Opening, Closure and Duration
What: Yearly ice opening, closure and duration. Note that the daily sea-ice concentration is in the PPv0 files.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/29_1/NOCLIM/TimeSeries/Initiation_Ice/

Example:  A2008_Opening_Ice.nc

## Mixed Layer Depth
What: Climatology of the mixed layer depth.

Where: /Volumes/output-prod/Takuvik/Teledetection/All/Clim/MLD/

Example: CLIM_MLD_180.nc

## Nsst
What: MODIS Sea-surface temperature.

Where: /Volumes/output-prod/Takuvik/Teledetection/Products/Nsst/

Example: 20160129/2008/180/A2008180_Nsst.nc

## PIC
What: Particulate Inorganic Carbon.

Where: /Volumes/output-prod/Takuvik/Teledetection/Products/pic/

Example: 20160129/2008/180/A2008180_pic.nc

## PPv0 files
What: These files contain many products reprojected on the equal-area grids of MODIS or SeaWiFS above 45 degrees North.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/

Example: 36_0_0/NOCLIM/2008/180/AM2008180_PP.nc

### Version 34_2_0
What: A preceding version.

The temporal coverage is 1 January 1998 to 31 December 2014. The reprocessing R2013.1 was used for MODIS-Aqua input files. The reprocessing R2010.0 was used for SeaWiFS input files. Sea ice concentration data from Cavalieri 1996 was used. See <http://nsidc.org/data/NSIDC-0051/versions/1>.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/34_2_0/NOCLIM/

### Version 35_0_0
What: A preceding version.

The temporal coverage is 1 January 2015 to 31 March 2016. The reprocessing R2014.0 was used for both MODIS-Aqua and SeaWiFS input files. Nsst, pic and pic_l2_flags were removed from this version to help data management. See Nsst or PIC section of this document tor their current location. Sea ice concentration data from Maslanik 1999 was used. See <http://nsidc.org/data/NSIDC-0081/versions/1>.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/35_0_0/NOCLIM/

### Version 36_0_0
What: The current version. This is a long term service version. It will be kept for a long time.

The temporal coverage is 1 January 1998 to 6 October 2016.  The reprocessing R2014.0 was used for both MODIS-Aqua and SeaWiFS input files. New a_w and b_bw values were used. Sea ice concentration data from Cavalieri 1996 was used for 2002 to 2015. See <http://nsidc.org/data/NSIDC-0051/versions/1>. Sea ice concentration data from Maslanik 1999 was used for 2016. See <http://nsidc.org/data/NSIDC-0081/versions/1>.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/36_0_0/NOCLIM/

Code and documentation: <http://doi.org/10.5281/zenodo.1003812>.

### Comparison between versions
The changes between the different versions don’t seem to produce big differences. See the images of the same day (13 August 2006) produced with each version: [AM2006225_PP_v0.34.2.0.jpg](https://gitlab.com/Takuvik/resources_public/blob/master/AM2006225_PP_v0.34.2.0.jpg), [AM2006225_PP_v0.35.0.0.jpg](https://gitlab.com/Takuvik/resources_public/blob/master/AM2006225_PP_v0.35.0.0.jpg) and [AM2006225_PP_v0.36.0.0.jpg](https://gitlab.com/Takuvik/resources_public/blob/master/AM2006225_PP_v0.36.0.0.jpg).

### Latest
What: A symbolic link pointing to the last version of the PPv0 files. It will be updated as new versions are added. It will always point the last stable version. The advantage is paths don’t have to be modified as new versions are added and deleted. Please note the disadvantage is it becomes impossible to retrace the specifications of the code producing a certain product by looking at the path of the PPv0 files.

Where: /Volumes/output-prod/Takuvik/Teledetection/Couleur/SORTIES/Latest/

## Rrs from GlobColour
What: L3BIN merged daily Rrs GlobColour on a full ISIN grid above 45 degrees North at the MODIS resolution (4.64 km). Please note that these files were processed at Takuvik. The original files directly downloaded from GlobColour are on /Volumes/taku-njall/GlobColour/GlobColour/.

Where: /Volumes/output-prod/GlobColourRrs/GlobColour_for_ppv1/v0.2/GlobColour_for_ppv1/R/

## sst_avhrr
What: Sea-surface temperature from AVHRR.

Where: /Volumes/output-prod/Takuvik/Teledetection/Products/sst_avhrr/

Example: 20160129/2008/180/A2008180_sst_avhrr.nc

## Note
Please note that even if a product is in the list of the products of a NetCDF file, it may not span the temporal or spatial coverture needed. Also, there can be  conditions on the computation of a product for each pixel too restrictive for some research projects. For exemple, the primary productivity is only computed when the sea ice concentration is below 10% in the PPv0 files in the current versions.

Please note that validating with the responsible of the data at Takuvik the existence or not of the data needed for a research project at the beginning stages of this research project allows the development of a research project with a higher chance of success.
