# Reinstall Yosemite 10_10

*This installation procedure was tested on Mac OS X Yosemite (10.10).*



<br>**Reference**

<https://support.apple.com/kb/PH18869?locale=en\_US>

<br>**Prerequistes**

1.  Internet connection.


<br>**Procedure**


1.  Restart holding down Command and R keys.

2.  Select Disk Utility and then click Continue.

3.  Select startup disk on the left, then click the Erase tab next to
    the S.O.S tab.

4.  From the Format menu, choose Mac OS Extended (Journaled).

5.  Enter the name of the partition you want to overwrite in the Name
    message box.

6.  Click Disk Utility in the bar menu (upper left corner) and then
    click Quit Disk Utility.

7.  Select Reinstall OS X and then click Continue.

8.  Follow the onscreen instructions. When you need to select a disk,
    select the Disk/Partition of your choice (In most cases there will
    be only one disk and no partition) and then click Install.

9.  Enter your Apple ID and password and then click Ok/Sign in.

10. Follow the onscreen instructions again.


